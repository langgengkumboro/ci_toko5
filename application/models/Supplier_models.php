<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Supplier_models extends CI_Model
{ 
	//panggil nama tabel
	private $_table = "supplier";
	public function rules(){
		return
		[
					[ 
						'field'=> 'kode_supplier',
						'label' => 'Kode Supplier',
						'rules' => 'required|max_length[5]',
						'errors' => [
							'requered' => 'kode supplier tidak boleh kosong.',
							'max_length' => 'kode supplier tidak boleh lebih dari 5 karakter.',
						]
					],
					[
						'field' => 'nama_supplier',
						'label'  => 'Nama Supplier',
						'rules' => 'required',
						'errors' =>[
							'required' => 'Nama Supplier tidak Boleh kosong.',
						]
					],
					[
						'field' => 'alamat',
						'label'  => 'Alamat',
						'rules' => 'required',
						'errors' =>[
							'required' => 'Alamat tidak Boleh kosong.',
						]
					],
					[
						'field' => 'telp',
						'label'  => 'Telp',
						'rules' => 'required',
						'errors' =>[
							'required' => 'Telp tidak Boleh kosong.',
							
						]
					]
					
		];
	}


	public function tampilDataSupplier()
		{
			//seperti : select * from <nama_table>
			return $this->db->get($this->_table)->result();
		}

	public function tampilDataSupplier2()
		{
			//KETIKA MAKE QUERY
			$query = $this->db->query("SELECT * FROM supplier WHERE flag = 1");
			return $query->result();
		}

	public function tampilDataSupplier3()
		{
			//MAKE QUERY BUILDER
			$this->db->select('*');
			$this->db->order_by('kode_supplier', 'ASC');
			$result = $this->db->get($this->_table);
			return $result->result();
		}
	public function save()
		{
			$data['kode_supplier'] =$this->input->post('kode_supplier');
			$data['nama_supplier'] =$this->input->post('nama_supplier');
			$data['alamat'] =$this->input->post('alamat');
			$data['telp'] =$this->input->post('telp');
			$data['flag'] =1;
			$this->db->insert($this->_table, $data);
		}

	public function detailsupplier($kode_supplier)
		{
			$this->db->select('*');
			$this->db->where('kode_supplier', $kode_supplier);
			$this->db->where('flag', 1);
			$result = $this->db->get($this->_table);
			return $result->result();
		}

	public function update($kode_supplier)
	{
		$data['nama_supplier']	= $this->input->post('nama_supplier');
		$data['alamat']			= $this->input->post('alamat');
		$data['telp']			= $this->input->post('telp');
		$data['flag']			= 1;
		
		$this->db->where('kode_supplier', $kode_supplier);
		$this->db->update($this->_table, $data);
	}
	public function delete($kode_supplier)
	
	{
		$this->db->where('kode_supplier',$kode_supplier);
		$this->db->delete($this->_table);
	}


} 