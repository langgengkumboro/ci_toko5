<?php
class Jenisbarang extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		//load model terkait
		$this->load->model("Jenisbarang_models");

		//cek sesi login
		$this->load->library('form_validation');
		//$user_login	= $this->session->userdata();
		//if()
		//load validasi
	
		//cek sesi login
		$user_login = $this->session->userdata();
		if(count($user_login) <= 1){
			redirect("auth/index", "refresh");
		}
	}

	public function index()
	{
		$this->data_jenisbarang();
	}
	public function data_jenisbarang()
	{
		$data['data_jenisbarang'] = $this->Jenisbarang_models->tampilDataJenisbarang();
		$data['content']	='forms/data_jenisbarang';
		$this->load->view('Home_2', $data);
	}

	public function detailjenisbarang($kode_jenis)
		{
			$data['data_jenisbarang'] =$this->Jenisbarang_models->detailjenisbarang($kode_jenis);
		    
		    $data['content']	='forms/detailjenisbarang';
			$this->load->view('Home_2', $data);
		}
	
	public function inputjenisbarang()
	{
		$data['data_jenisbarang'] = $this->Jenisbarang_models->tampilDataJenisbarang();

		//if (!empty($_REQUEST)){
		//	$m_jenisbarang = $this->Jenisbarang_models;
		//	$m_jenisbarang->save();
		//	redirect("Jenisbarang/index", "refresh");
		$validation = $this->form_validation;
		$validation->set_rules($this->Jenisbarang_models->rules());

		if ($validation->run()){
			$this->Jenisbarang_models->save();
			$this->session->set_flashdata('info', '<div style="color : green">SIMPAN DATA BERHASIL HOREEE </div>');
			redirect("Jenisbarang/index", "refresh");
		}


		$data['content']	='forms/inputjenisbarang';
		$this->load->view('Home_2', $data);
	}
	
	public function editjenisbarang($kode_jenis)
	{	
		$data['data_jenisbarang'] = $this->Jenisbarang_models->tampilDataJenisbarang();
		$data['data_jenisbarang']	= $this->Jenisbarang_models->detailjenisbarang($kode_jenis);
		
		//if (!empty($_REQUEST)) {
		//		$m_jenisbarang = $this->Jenisbarang_models;
		//		$m_jenisbarang->update($kode_jenis);
		//		redirect("Jenisbarang/index", "refresh");	
		//	}
		//$this->load->view('editjenisbarang', $data);

		$validation = $this->form_validation;
		$validation->set_rules($this->Jenisbarang_models->rules());

		if ($validation->run()){
			$this->Jenisbarang_models->update($kode_jenis);
			$this->session->set_flashdata('info', '<div style="color : green">UPDATE DATA BERHASIL HOREEE </div>');
			redirect("Jenisbarang/index", "refresh");
		}


		$data['content']	='forms/editjenisbarang';
		$this->load->view('Home_2', $data);	
	}

	public function delete($kode_jabatan)
	{
		$m_jenisbarang = $this->Jenisbarang_models;
		$m_jenisbarang->delete($kode_jenis);
		redirect("Jenisbarang/index", "refresh");	
	}

}

