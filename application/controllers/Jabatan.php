<?php
class Jabatan extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		//load model terkait
		$this->load->model("Jabatan_models");

		//load validasi
		$this->load->library('form_validation');

		//cek sesi login
		//$user_login	= $this->session->userdata();
		//if()
		//load validasi
	
		//cek sesi login
		$user_login = $this->session->userdata();
		if(count($user_login) <= 1){
			redirect("auth/index", "refresh");
		}
	}

	public function index()
	{
		$this->data_jabatan();
	}
	public function data_jabatan()
	{
		$data['data_jabatan'] = $this->Jabatan_models->tampilDataJabatan();
		$data['content']	='forms/data_jabatan';
		$this->load->view('Home_2', $data);
	}
	public function detailjabatan($kode_jabatan)
	{
		$data['data_jabatan'] =$this->Jabatan_models->detail($kode_jabatan);
		$data['content']       ='forms/detailjabatan';
		$this->load->view('home_2', $data);
	}

	public function inputdatajabatan()
	{
		$data['data_jabatan'] = $this->Jabatan_models->tampilDataJabatan();

		//if (!empty($_REQUEST)){
		//	$m_jabatan = $this->Jabatan_models;
		//	$m_jabatan->save();
		//	redirect("Jabatan/index", "refresh");
		//}
		$validation = $this->form_validation;
		$validation->set_rules($this->Jabatan_models->rules());

		if ($validation->run()){
			$this->Jabatan_models->save();
			$this->session->set_flashdata('info', '<div style="color : green">SIMPAN DATA BERHASIL HOREEE </div>');
			redirect("Jabatan/index", "refresh");
			}

			
			$data['content']	='forms/inputdatajabatan';
		$this->load->view('Home_2', $data);


	}
	public function editjabatan($kode_jabatan)
	{	
		$data['data_jabatan'] = $this->Jabatan_models->detail($kode_jabatan);
		
		//if (!empty($_REQUEST)) {
		//		$m_jabatan = $this->Jabatan_models;
		//		$m_jabatan->update($kode_jabatan);
		//		redirect("Jabatan/index", "refresh");	
		//	}
		
		//$this->load->view('editjabatan', $data);
		$validation = $this->form_validation;
		$validation->set_rules($this->Jabatan_models->rules());

		if ($validation->run()){
			$this->Jabatan_models->update($kode_jabatan);
			$this->session->set_flashdata('info', '<div style="color : green">UPDATE DATA BERHASIL HOREEE </div>');
			redirect("Jabatan/index", "refresh");
			}

			
			$data['content']	='forms/inputdatajabatan';
		$this->load->view('Home_2', $data);	
	}

	public function delete($kode_jabatan)
	{
		$m_jabatan = $this->Jabatan_models;
		$m_jabatan->delete($kode_jabatan);	
		redirect("Jabatan/index", "refresh");	
	}



}

