<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Barang extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		//load model terkait
		$this->load->model("Barang_models");
		$this->load->model("Jenisbarang_models");

		//load validasi
		$this->load->library('form_validation');

		//cek sesi login
		//$user_login	= $this->session->userdata();
		//if()
		//load validasi
	
		//cek sesi login
		$user_login = $this->session->userdata();
		if(count($user_login) <= 1){
			redirect("auth/index", "refresh");
		}
	}

	public function index()
	{
		$this->data_barang();
	}
	public function data_barang()
	{
		$data['data_barang'] = $this->Barang_models->tampilDataBarang();
		$data['content']	='forms/data_barang';
		$this->load->view('Home_2', $data);
	}
	public function detailbarang($kode_barang)
	{
		$data['data_barang'] =$this->Barang_models->detail($kode_barang);
		
		$data['content']	='forms/detailbarang';
		$this->load->view('Home_2', $data);
	}

	public function inputbarang()
	{
		$data['data_jenisbarang'] = $this->Jenisbarang_models->tampilDataJenisbarang();

		//if (!empty($_REQUEST)){
		//	$m_barang = $this->Barang_models;
			//$m_barang->save();
			//redirect("Barang/index", "refresh");

		//validasi terlebih dahulu
		$validation = $this->form_validation;
		$validation->set_rules($this->Barang_models->rules());

		if ($validation->run()){
			$this->Barang_models->save();
			$this->session->set_flashdata('info', '<div style="color : green">SIMPAN DATA BERHASIL HOREEE </div>');
			redirect("Barang/index", "refresh");
			}

			//$data['content']	= 'forms/input_barang';
			//$this->load->view('Home_2',$data);

		

			$data['content']	='forms/inputbarang';
		$this->load->view('Home_2', $data);
		
	}
	
	public function editbarang($kode_barang)
	{	
		$data['data_jenisbarang'] = $this->Jenisbarang_models->tampilDataJenisbarang();
		$data['detail_barang']	= $this->Barang_models->detail($kode_barang);

		//if (!empty($_REQUEST)){
		//	$m_barang = $this->Barang_models;
		//	$m_barang->update($kode_barang);
		//	redirect("Barang/index", "refresh");
		$validation = $this->form_validation;
		$validation->set_rules($this->Barang_models->rules());

		if ($validation->run()){
			$this->Barang_models->update($kode_barang);
			$this->session->set_flashdata('info', '<div style="color : green">UPDATE DATA BERHASIL HOREEE </div>');
			redirect("Barang/index", "refresh");
		}
		$data['content']	='forms/editbarang';
		$this->load->view('Home_2', $data);	
	}
	public function delete($kode_barang)
	{
		$m_barang = $this->Barang_models;
		$m_barang->delete($kode_barang);	
		redirect("Barang/index", "refresh");	
	}
	

}
